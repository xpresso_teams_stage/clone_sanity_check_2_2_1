__all__ = ['XprEmailSender']
__author__ = 'Srijan Sharma'

import smtplib
from email.headerregistry import Address
from email.message import EmailMessage

from xpresso.ai.core.commons.exceptions.xpr_exceptions import EmailException
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger


class XprEmailSender:

    EMAIL_SECTION = "email_notification"
    PROJECT_NAME = "project_name"
    SMTP_HOST = "smtphost"
    SMTP_PORT = "smtpport"
    SENDER_MAIL = "sender_mail"
    SENDER_PASSWD = "sender_passwd"

    def __init__(self):
        self.xpr_config = XprConfigParser()
        self.logger = XprLogger()

    def send_single_mail(self, receiver, message, subject, html_content=None):
        smtp_session = None
        try:
            smtp_session = smtplib.SMTP(self.xpr_config[self.EMAIL_SECTION][
                                            self.SMTP_HOST], self.xpr_config[
                                            self.EMAIL_SECTION][self.SMTP_PORT])
            smtp_session.starttls()

            smtp_session.login(self.xpr_config[self.EMAIL_SECTION][self.SENDER_MAIL],
                               self.xpr_config[self.EMAIL_SECTION][self.SENDER_PASSWD])
            msg = EmailMessage()
            msg['From'] = self.xpr_config[self.EMAIL_SECTION][self.SENDER_MAIL]
            msg['To'] = Address(display_name=receiver['display_name'],
                                username=receiver['username'],
                                domain=receiver['domain'])
            msg['Subject'] = subject
            msg.set_content(message)
            if html_content is not None:
                msg.add_alternative(html_content, subtype='html')
            smtp_session.send_message(msg)
            self.logger.info("mail Successfully sent to {}".format(receiver))

        except smtplib.SMTPServerDisconnected as e:
            err = "api_server unexpectedly disconnects:{}".format(e)
            self.logger.error(err)
            raise EmailException(err)

        except smtplib.SMTPSenderRefused as e:
            err = "Sender address refused : {}".format(e)
            self.logger.error(err)
            raise EmailException(err)

        except smtplib.SMTPRecipientsRefused as e:
            err = "recipient {} addresses refused : {}".format(receiver,e)
            self.logger.error(err)
            raise EmailException(err)

        except smtplib.SMTPDataError as e:
            err = "The SMTP api_server refused to accept the message data. :{}".format(e)
            self.logger.error(err)
            raise EmailException()

        except smtplib.SMTPConnectError as e:
            err = "Error connecting to api_server : {}".format(e)
            self.logger.error(err)
            raise EmailException(err)

        except smtplib.SMTPAuthenticationError as e:
            err = "Unable to authenticate : {}".format(e)
            self.logger.error(err)
            raise  EmailException(err)

        except smtplib.SMTPException as e:
            err = "Error sending mail :{}".format(e)
            self.logger.error(err)
            raise EmailException(err)

        finally:
            if smtp_session is not None:
                smtp_session.quit()

    def send(self, receivers, message, subject, html_content=None):
        success = list()
        failure = list()
        for receiver in receivers:
            try:
                self.send_single_mail(receiver, message, subject, html_content)
                success.append(receiver)
            except EmailException as e:
                failure.append(receiver)
        self.logger.info('successfully send email to {} '.format(str(success)))
        self.logger.info('Unable to send email to {} '.format(str(failure)))
        return failure


if __name__ == "__main__":
    sender = XprEmailSender()
    sender.send(["##"], "Test Message", "Test subject")
