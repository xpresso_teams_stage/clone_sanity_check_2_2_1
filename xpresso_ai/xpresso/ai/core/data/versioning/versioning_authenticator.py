__all__ = ['VersioningAuthenticator']
__author__ = 'Gopi Krishna'

import os
from os import path
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import AuthenticationFailedException, InvalidEnvironmentException, \
    RepoPermissionException
from xpresso.ai.core.commons.utils.generic_utils import str_hash256
from xpresso.ai.core.commons.utils.constants import config_paths,\
    USER_CONTEXT, CONTEXT_PROJECT_LIST_KEY, CONTEXT_USER_INFO_KEY, \
    PRIMARY_ROLE, SUPER_USER, DV_PROJECT_TOKEN, DV_USER_TOKEN_FLAG, \
    DV_PROJECT_TOKEN_FLAG, CONTROLLER_FIELD, CLIENT_PATH_FIELD, \
    SERVER_URL_FIELD, PROJECT_TOKEN_IN_PROJECTS, DV_INPUT_REPO_NAME, \
    PROJECT_NAME_IN_PROJECTS, DV_SERVER_REQUEST_FLAG, \
    DV_SERVER_AUTH_TOKEN, DEFAULT_WORKSPACE, UID
from xpresso.ai.core.data.versioning.dv_field_name import \
    DataVersioningFieldName as DvFields
from xpresso.ai.core.commons.utils.user_utils \
    import get_token as get_user_token


class VersioningAuthenticator:
    """
    Authenticates the requests for using Data versioning module
    """
    def __init__(self, config: XprConfigParser = XprConfigParser(), **kwargs):
        """
        Class constructor
        """
        self.base_path = path.join(
            path.expanduser('~'),
            config[CONTROLLER_FIELD][CLIENT_PATH_FIELD]
        )
        self.kwargs = kwargs if kwargs else {}
        # Fetching config based on workspace is deprecated
        self.config = config
        self.token_flag = DV_USER_TOKEN_FLAG

    def init(self):
        if DV_PROJECT_TOKEN in self.kwargs:
            self.token_flag = DV_PROJECT_TOKEN_FLAG
        elif DV_SERVER_AUTH_TOKEN in self.kwargs:
            self.token_flag = DV_SERVER_REQUEST_FLAG
        else:
            # In case no kwargs are provided, it is assumed the request
            # is sent through xprctl or jupyter notebook.
            self.token_flag = DV_USER_TOKEN_FLAG

    def get_token(self):
        """Token is saved in the local file system for """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            return self.kwargs[DV_PROJECT_TOKEN]
        elif self.token_flag == DV_SERVER_REQUEST_FLAG:
            return self.kwargs[DV_SERVER_AUTH_TOKEN]

        token_path = os.path.join(self.base_path, ".current")
        return get_user_token(token_path)

    def get_workspace_env(self):
        try:
            current_user = os.getenv('CURRENT_USER', '')
            if current_user:
                current_user_enc = str_hash256(current_user)
                workspace_file = os.path.join(self.base_path, current_user_enc,
                                              '.workspace')
            else:
                workspace_file = os.path.join(self.base_path, '.workspace')
            with open(workspace_file, "r") as env_file:
                env = env_file.read()
        except FileNotFoundError:
            return DEFAULT_WORKSPACE
        return env

    def check_token(self, token):
        """
        checks if the token is valid or not
        Args:
            token: login token saved on user system
        Returns:
            returns response for token authentication request from server
        """
        server_endpoint = self.config[CONTROLLER_FIELD][SERVER_URL_FIELD]
        print(token)
        if self.token_flag == DV_USER_TOKEN_FLAG:
            url = f"{server_endpoint}/versioning/auth"
            request_header = {"token": token}
            request_body = {}
        elif self.token_flag == DV_SERVER_REQUEST_FLAG:
            url = "https://127.0.0.1:5050/versioning/auth"
            request_header = {"token": token}
            request_body = {}
        else:
            url = f"{server_endpoint}/project/api"
            request_header = {}
            request_body = {
                PROJECT_TOKEN_IN_PROJECTS: token
            }
        token_response = SendHTTPRequest().send(
            url=url, http_method=HTTPMethod.GET,
            header=request_header,
            data=request_body
        )
        return token_response

    def authenticate_session(self):
        """
        authenticates the login session by checking if there is a login
        """
        if self.token_flag != DV_USER_TOKEN_FLAG:
            return
        token = self.get_token()
        self.check_token(token)

    def authenticate_request(self, caller_method):
        """
        authenticates any new request to data versioning module
        that needs access level and login expiry checks
        """
        def auth_decorator(*args, **kwargs):
            # If project token is used, then no need to test this
            if self.token_flag == DV_PROJECT_TOKEN_FLAG:
                return caller_method(*args, **kwargs)
            token = self.get_token()
            token_response = self.check_token(token)
            token_user_id = self.get_uid_from_token(token_response)
            if kwargs and len(kwargs):
                kwargs["request_uid"] = token_user_id
            temp_repo_name = self.fetch_repo_name_from_request(*args, **kwargs)
            if temp_repo_name:
                self.validate_repo_access(
                    temp_repo_name,
                    token_response
                )
            return caller_method(*args, **kwargs)
        return auth_decorator

    def validate_repo_access(self, repo_name: str, token_response: dict):
        """
        validates if the user has access to the repo or not
        By default, xpresso super user has access to everything

        :param repo_name:
            name of the repo
        :param token_response:
            response received after sending request to server
        """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            project_name = token_response[PROJECT_NAME_IN_PROJECTS]
            if project_name != repo_name:
                raise RepoPermissionException()
            return
        user_info = token_response[USER_CONTEXT][CONTEXT_USER_INFO_KEY]
        if user_info[PRIMARY_ROLE].lower() == SUPER_USER:
            return
        project_list = token_response[USER_CONTEXT][CONTEXT_PROJECT_LIST_KEY]
        if repo_name not in project_list:
            raise RepoPermissionException(
                "Either you do not have access to this repo"
                " or it does not exist."
            )

    def filter_repo(self, caller_method):
        """
        decorator method to filter the output for list_repo
        Args:
            caller_method:
        Returns:
             returns list of repos
        """
        def filter_decorator(*args, **kwargs):
            token = self.get_token()
            # validates token
            token_response = self.check_token(token)
            repo_list_info = caller_method(*args, **kwargs)
            if self.token_flag == DV_PROJECT_TOKEN_FLAG:
                filter_repo_name = token_response[PROJECT_NAME_IN_PROJECTS]
                filter_repo_info = []
                for repo_item in repo_list_info:
                    if repo_item[DV_INPUT_REPO_NAME] == filter_repo_name:
                        filter_repo_info.append(repo_item)
                        break
                if not len(filter_repo_info):
                    raise RepoPermissionException
                return filter_repo_info
            user_info = token_response[USER_CONTEXT][CONTEXT_USER_INFO_KEY]
            if user_info[PRIMARY_ROLE].lower() == SUPER_USER:
                return repo_list_info
            project_list = \
                token_response[USER_CONTEXT][CONTEXT_PROJECT_LIST_KEY]
            filtered_repo_list = []
            for repo_item in repo_list_info:
                if repo_item[DV_INPUT_REPO_NAME] in project_list:
                    filtered_repo_list.append(repo_item)
            if not len(filtered_repo_list):
                raise RepoPermissionException(
                    "Unable to find any repos you can access"
                )
            return filtered_repo_list
        return filter_decorator

    def get_uid_from_token(self, token_response: dict):
        """
        fetches the uid of the user from token_response.
        token_response is the output of the token validation request

        Args:
            token_response: response from token validation request
        """
        if self.token_flag == DV_PROJECT_TOKEN_FLAG:
            token_user = DvFields.DV_PROJECT_TOKEN_USER.value
            project_token_uid = self.kwargs[token_user] if token_user in \
                self.kwargs else "xpresso_platform"
            return project_token_uid
        user_info = token_response[USER_CONTEXT][CONTEXT_USER_INFO_KEY]
        uid = user_info[UID]
        return uid

    @staticmethod
    def fetch_repo_name_from_request(*args, **kwargs):
        """
        fetch repo name provided as input as part of the request
        """
        # kwargs is the common input method currently used in data versioning
        if DV_INPUT_REPO_NAME in kwargs:
            return kwargs[DV_INPUT_REPO_NAME]
        # If repo_name is not provided in kwargs and provided as part of a dict
        for arg in args:
            # positional arguments cannot be verified unless it is a dict
            if type(arg) is dict and DV_INPUT_REPO_NAME in arg:
                return arg[DV_INPUT_REPO_NAME]
        # If repo_name is not found in kwargs & args, repo name validation
        # should be handled by the request method itself
        return None
